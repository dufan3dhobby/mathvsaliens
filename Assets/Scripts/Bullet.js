﻿#pragma strict

public var speed : int;

public var initialPosition : Vector3;

function Start () 
{
	initialPosition = this.gameObject.transform.position;
}

function Update () 
{
	MoveBullet();
	
	if (this.gameObject.transform.localPosition.y >= 4.75f)
	{
		speed = 0;
		ResetPosition();
		
		//generate new number
		if (this.gameObject.name == "Bullet_1" || this.gameObject.name == "Bullet_3" || this.gameObject.name == "Bullet_5")
		{
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(2);
		}
		else
		{
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(1);
		}
	}
}

function MoveBullet ()
{
	//this.gameObject.GetComponent(Rigidbody).velocity.y = speed;
	this.gameObject.transform.position.y = this.gameObject.transform.position.y + speed * Time.deltaTime;
}

function ResetPosition ()
{
	this.gameObject.transform.position = initialPosition;
}