private var originPosition : Vector3;
private var originRotation : Quaternion;
 
private var shake_decay: float;
private var shake_intensity: float;
 
function Start()
{
	originPosition = transform.position;
	originRotation = transform.rotation;
	
	/*Shake();
	
	yield WaitForSeconds(5f);
	
	transform.position = originPosition;
 	transform.rotation = originRotation;*/
} 
 
function Update(){
    if(shake_intensity > 0){
        transform.position = originPosition + Random.insideUnitSphere * shake_intensity;
        transform.rotation =  Quaternion(
                        originRotation.x + Random.Range(-shake_intensity,shake_intensity)*.2,
                        originRotation.y + Random.Range(-shake_intensity,shake_intensity)*.2,
                        originRotation.z + Random.Range(-shake_intensity,shake_intensity)*.2,
                        originRotation.w + Random.Range(-shake_intensity,shake_intensity)*.2);
        shake_intensity -= shake_decay;
    }
}
 
function Shake()
{
    originPosition = transform.position;
    originRotation = transform.rotation;
    shake_intensity = 0.07f;
    shake_decay = 0.002f;
}