﻿#pragma strict

function Start () 
{

}

function Update () 
{

}

function OnMouseDown ()
{
	print("Mouse Down on Start Button!");
	
	GameObject.Find("Button_Active_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.localScale.x = this.gameObject.transform.localScale.x * 1.1f;
	this.gameObject.transform.localScale.y = this.gameObject.transform.localScale.y * 1.1f;
	this.gameObject.transform.localScale.z = this.gameObject.transform.localScale.z * 1.1f;
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Start Button!");
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.localScale.x = 0.7f;
	this.gameObject.transform.localScale.y = 0.7f;
	this.gameObject.transform.localScale.z = 0.7f;
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(true);
	
	BlinkStartButton();
	
	yield WaitForSeconds(0.8f);
	
	GameObject.Find("BGM_Title").GetComponent(AudioSource).Stop();
	
	HOTween.To(GameObject.Find("Titles").transform, 0.5f, "localPosition", Vector3(0f,5f,0f), true, EaseType.EaseOutBack, 0f);
	
	GameObject.Find("Kekkai").transform.FindChild("Collider").gameObject.SetActive(true);
	
	HOTween.To(GameObject.Find("Enemies").transform, 0.5f, "localPosition", Vector3(0f,-2f,0f), true, EaseType.EaseInBack, 0f);
	
	HOTween.To(GameObject.Find("Score_Bar").transform, 0.5f, "localPosition", Vector3(0f,-1f,0f), true, EaseType.EaseInBack, 0f);
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(1); //reset number
	
	HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(1.6f,1.6f,1.6f), true, EaseType.EaseOutBack, 0f);	
	//GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.transform.localPosition.y = 2.42f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).characterSize = 0.15f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).text = "Match\nnumbers to\n shoot!";
	
	yield WaitForSeconds(0.5f);
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).CountDown();
	
	//GameObject.Find("Titles").SetActive(false);
	//GameObject.Find("Title_Controller").GetComponent(Title_Controller).loadings.SetActive(true);
	
	//Application.LoadLevel("Gameplay");
}

function OnMouseUp ()
{
	print("Mouse Up on Start Button!");
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.localScale.x = 0.7f;
	this.gameObject.transform.localScale.y = 0.7f;
	this.gameObject.transform.localScale.z = 0.7f;
}

function BlinkStartButton ()
{
	this.gameObject.GetComponent(SpriteRenderer).color.a = 0f;
	
	yield WaitForSeconds(0.1f);
	
	this.gameObject.GetComponent(SpriteRenderer).color.a = 1f;
	
	yield WaitForSeconds(0.1f);
	
	this.gameObject.GetComponent(SpriteRenderer).color.a = 0f;
	
	yield WaitForSeconds(0.1f);
	
	this.gameObject.GetComponent(SpriteRenderer).color.a = 1f;
	
	yield WaitForSeconds(0.1f);
	
	this.gameObject.GetComponent(SpriteRenderer).color.a = 0f;
	
	yield WaitForSeconds(0.1f);
	
	this.gameObject.GetComponent(SpriteRenderer).color.a = 1f;
}