﻿#pragma strict

public var selectedMaterial : Material;
public var unselectedMaterial : Material;


function Start () 
{

}

function Update () 
{

}

function OnMouseDown ()
{
	print("Mouse Down on Retry Button!");
	
	GameObject.Find("Button_Active_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 1.0f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 1.0f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 1.0f;
	
	this.gameObject.GetComponent(MeshRenderer).material = selectedMaterial;
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Retry Button!");
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(true);
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
	
	//Application.LoadLevel(Application.loadedLevel);
	
	GameObject.Find("Kekkais").transform.FindChild("Kekkai").gameObject.GetComponent(Animator).CrossFade ("None", 0.01f);
	GameObject.Find("Professor_Cursing").transform.FindChild("Cursing").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	GameObject.Find("Professor_Right").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	GameObject.Find("Professor_Left").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	
	HOTween.To(GameObject.Find("Pauses").transform, 0.5f, "position", Vector3(0,10f,0f), true, EaseType.EaseInBack, 0f);
	
	HOTween.To(GameObject.Find("Results").transform, 0.5f, "position", Vector3(0,10f,0f), true, EaseType.EaseInBack, 0f);

	GameObject.Find("BGM_Gameplay").GetComponent(AudioSource).Stop();
	
	GameObject.Find("BGM_Lose").GetComponent(AudioSource).Stop();

	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).currentScore = 0f;
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).isGameStarted = false;
	
	HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(1.6f,1.6f,1.6f), true, EaseType.EaseOutBack, 0f);	
	//GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.transform.localPosition.y = 2.42f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).characterSize = 0.15f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).text = "Match\nnumbers to\n shoot!";
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).ResetEnemies();
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNumbersForAliens();
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyLeftIndexToHit = 0;
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyRightIndexToHit = 0;
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(0); //reset number
	
	yield WaitForSeconds(0.5f);
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).CountDown();
	
	HOTween.To(GameObject.Find("Enemies").transform, 0.5f, "localPosition", Vector3(0f,0.35f,-4.84f), false, EaseType.EaseInBack, 0f);
	HOTween.To(GameObject.Find("Enemy_Right").transform, 0.5f, "localPosition", Vector3(0.76f,-2.48f,0.15f), false, EaseType.EaseInBack, 0f);
	HOTween.To(GameObject.Find("Enemy_Left").transform, 0.5f, "localPosition", Vector3(-0.76f,-2.48f,0.15f), false, EaseType.EaseInBack, 0f);
}

function OnMouseUp ()
{
	print("Mouse Up on Retry Button!");
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	GameObject.Find("Retry").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
}