﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoBehaviour 
{
	GameObject player;
	GameObject enemy;
	public Sprite img;

	// Use this for initialization
	void Start () 
	{
		//Creating a new gameobject named New One and put a SpriteRenderer component in it
		player = new GameObject ("New One");
		player.transform.position = new Vector3(2f, 3f, 1f);
		player.AddComponent <SpriteRenderer> ();
		player.GetComponent <SpriteRenderer> ().sprite = img;

		//Make a clone of EnemyPrefab that is located in the Resources folder
		enemy = Instantiate (Resources.Load ("Prefabs/EnemyPrefab")) as GameObject;
		enemy.name = "Enemy_1"; //change the name of the clone to Enemy_1   37
	}
	
	// Update is called once per frame
	void Update () 
	{
	
	}
}
