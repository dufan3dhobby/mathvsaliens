﻿#pragma strict

function Start () 
{

}

function Update () 
{
	TransitionFromMorningToDusk();
}

function TransitionFromMorningToDusk ()
{
	if (this.gameObject.GetComponent(MeshRenderer).material.color.b >= 0.5f && this.gameObject.GetComponent(MeshRenderer).material.color.r >= 1f && this.gameObject.GetComponent(MeshRenderer).material.color.g >= 1f)
	{
		this.gameObject.GetComponent(MeshRenderer).material.color.b = this.gameObject.GetComponent(MeshRenderer).material.color.b - 0.03 * Time.deltaTime;
	}
	else
	{
		TransitionFromMorningToEvening ();
	}
}

function TransitionFromMorningToEvening ()
{
	if (this.gameObject.GetComponent(MeshRenderer).material.color.g >= 0.5f && this.gameObject.GetComponent(MeshRenderer).material.color.r >= 1f && this.gameObject.GetComponent(MeshRenderer).material.color.b < 0.5f)
	{
		this.gameObject.GetComponent(MeshRenderer).material.color.g = this.gameObject.GetComponent(MeshRenderer).material.color.g - 0.03 * Time.deltaTime;
	}
	else
	{
		TransitionFromEveningToMidnight ();
	}
}

function TransitionFromEveningToMidnight ()
{
	if (this.gameObject.GetComponent(MeshRenderer).material.color.r >= 0.25f && this.gameObject.GetComponent(MeshRenderer).material.color.g < 0.5f && this.gameObject.GetComponent(MeshRenderer).material.color.b < 0.5f)
	{
		this.gameObject.GetComponent(MeshRenderer).material.color.r = this.gameObject.GetComponent(MeshRenderer).material.color.r - 0.05 * Time.deltaTime;
	}
	else
	{
		TransitionFromEveningToMorning ();
	}
}

function TransitionFromEveningToMorning ()
{
	if (this.gameObject.GetComponent(MeshRenderer).material.color.r < 1f || this.gameObject.GetComponent(MeshRenderer).material.color.g < 1f || this.gameObject.GetComponent(MeshRenderer).material.color.b < 1f)
	{
		this.gameObject.GetComponent(MeshRenderer).material.color.r = this.gameObject.GetComponent(MeshRenderer).material.color.r + 0.03 * Time.deltaTime;
		this.gameObject.GetComponent(MeshRenderer).material.color.g = this.gameObject.GetComponent(MeshRenderer).material.color.g + 0.03 * Time.deltaTime;
		this.gameObject.GetComponent(MeshRenderer).material.color.b = this.gameObject.GetComponent(MeshRenderer).material.color.b + 0.03 * Time.deltaTime;
	}
	else
	{
		TransitionFromMorningToDusk ();
	}
}