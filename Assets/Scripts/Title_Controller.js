﻿#pragma strict

import Holoville.HOTween;
import Holoville.HOTween.Plugins; 

public var number1NeedToMatch : int;
public var number2NeedToMatch : int;
public var totalNumberSelected : int;

public var isTutorialShowed : boolean;

public var tutorials : GameObject;
public var loadings : GameObject;

function Start () 
{
	GenerateNewNumber();
	
	GameObject.Find("Highscore").transform.FindChild("Text").gameObject.GetComponent(TextMesh).text = "" + PlayerPrefs.GetInt("Highscore"); //set highscore text
}

function Update () 
{
	//update the number
	GameObject.Find("Computer_1").transform.FindChild("Number").gameObject.GetComponent(TextMesh).text = "" + number1NeedToMatch;
	GameObject.Find("Computer_2").transform.FindChild("Number").gameObject.GetComponent(TextMesh).text = "" + number2NeedToMatch;

	if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) 
    {
    	if (!isTutorialShowed)
    	{
    		Application.Quit();
    	}
    	else
    	{
    		HideTutorialScreen();
    	}
    }
}

function GenerateNewNumber () //0 for beginning, 1 after hitting right, 2 after hitting left
{
	number1NeedToMatch = Random.Range(7,13);
	
	number2NeedToMatch = Random.Range(7,13);

	if (number1NeedToMatch >= number2NeedToMatch)
	{
		var number1 : int = Random.Range(2,number2NeedToMatch-2);
	}
	else
	{
		number1 = Random.Range(2,number1NeedToMatch-2);
	}
	
	var number2 : int = number1NeedToMatch - number1;
	var number3 : int = number2NeedToMatch - number1;
	var number4 : int = number1 + 2;
	var number5 : int = number1 - 1;
	
	var index : int = Random.Range(1,5);
	
	if (index == 1)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
	}
	else
	if (index == 2)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
	}
	else
	if (index == 3)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
	}
	else
	if (index == 4)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
	}
}

function ShowTutorialScreen ()
{
	isTutorialShowed = true;
	
	tutorials.SetActive(true);
}

function HideTutorialScreen ()
{
	isTutorialShowed = false;
	
	tutorials.SetActive(false);
}