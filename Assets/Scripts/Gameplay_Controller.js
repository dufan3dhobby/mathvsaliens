﻿#pragma strict

import Holoville.HOTween;
import Holoville.HOTween.Plugins; 

public var number1NeedToMatch : int;
public var number2NeedToMatch : int;
public var totalNumberSelected : int;
public var currentScore : int;
public var lastEnemyRightIndexToHit : int;
public var lastEnemyLeftIndexToHit : int;

//public var comboChecker = new Array (int);

public var kekkaiBlinkingSpeed : float;
public var timerForBlink : float;

public var unselectedMonitorMaterial : Material;
public var selectedMonitorMaterial : Material;

public var isGameStarted : boolean;
public var isZeroBlink : boolean;
public var isCountDownFinised : boolean;
public var is3ConsecutiveCannonOwned : boolean;
public var is2InARowCannonOwned : boolean;
public var isStunnedCannonOwned : boolean;
public var isTimerForBlinkStarted : boolean;

public var kekkaiGameObject : GameObject;
public var enemies : GameObject;
public var enemiesDummy : GameObject;
public var solution1 : GameObject;
public var solution2_1 : GameObject;
public var solution2_2 : GameObject;

public var currentScoreText : TextMesh;

private var tempColor : Color;

function Start () 
{
	GameObject.Find("Highscore").transform.FindChild("Text").gameObject.GetComponent(TextMesh).text = "" + PlayerPrefs.GetInt("Highscore"); //set highscore text

	GenerateNumbersForAliens();

	GenerateNewNumber(0);
	
	ActivateAnimatorForEnemiesLeft();
	ActivateAnimatorForEnemiesRight();
	
	//CountDown();
}

function Update () 
{
	currentScoreText.text = "" + currentScore;

	/*if (comboChecker.length > 1)
	{
		print("Combochecker length: " + comboChecker.length);
		print("Combochecker: " + comboChecker);
	
		if (comboChecker[comboChecker.length-1] != comboChecker[comboChecker.length-2])
		{
			comboChecker.RemoveAt(comboChecker.length-2);
		}
	}*/
	
	//check if the combo is done or not
	/*if (comboChecker.length > 2)
	{
		if ((comboChecker[0] == 1 && comboChecker[1] == 1 && comboChecker[2] == 1) || (comboChecker[0] == 3 && comboChecker[1] == 3 && comboChecker[2] == 3) || (comboChecker[0] == 2 && comboChecker[1] == 2 && comboChecker[2] == 2))
		{
			print("Stun!");
			
			//comboChecker.RemoveAt(1);
			comboChecker.RemoveAt(1);
			comboChecker.RemoveAt(1);
			
			comboChecker[0] = 0;
		}
	}*/

	if (isTimerForBlinkStarted && isGameStarted)
	{
		RunTimerForBlink();
	}

	if (number1NeedToMatch == totalNumberSelected)
	{
		isTimerForBlinkStarted = false; //stop the timer
		timerForBlink = 0f;
	
		currentScore = currentScore + number1NeedToMatch; //add score
	
		number1NeedToMatch = number1NeedToMatch + 20;
	
		//Debug.Log("Shoot Left Cannon");
	
		GameObject.Find("Main Camera").GetComponent(Camera_Shake).Shake();
	
		//print("Shoot Bullet1!");
		
		GameObject.Find("Cursing").GetComponent(Animator).CrossFade ("Command", 0.01f);
		
		HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(0.7f,0.7f,0.7f), true, EaseType.EaseOutBack, 0f);
		
		GameObject.Find("Shoot_SFX").GetComponent(AudioSource).Play();
		
		ShootCannon1(1);
		
		ResetScaleDialogueBalloon();
	}
	
	if (number2NeedToMatch == totalNumberSelected)
	{
		isTimerForBlinkStarted = false; //stop the timer
		timerForBlink = 0f;
	
		currentScore = currentScore + number2NeedToMatch; //add score
	
		number2NeedToMatch = number2NeedToMatch + 20;
	
		//Debug.Log("Shoot Right Cannon");
	
		GameObject.Find("Main Camera").GetComponent(Camera_Shake).Shake();
	
		//print("Shoot Bullet2!");
		
		GameObject.Find("Cursing").GetComponent(Animator).CrossFade ("Command_Left", 0.01f);
		
		HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(0.7f,0.7f,0.7f), true, EaseType.EaseOutBack, 0f);
		
		GameObject.Find("Shoot_SFX").GetComponent(AudioSource).Play();
		
		ShootCannon2(2);
		
		ResetScaleDialogueBalloon();
	}
	
	//update the number
	//GameObject.Find("Computer_1").transform.FindChild("Number").gameObject.GetComponent(TextMesh).text = "" + number1NeedToMatch;
	//GameObject.Find("Computer_2").transform.FindChild("Number").gameObject.GetComponent(TextMesh).text = "" + number2NeedToMatch;

	tempColor = kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.GetColor("_TintColor");

	if (tempColor.a < 0.3f)
	{
		isZeroBlink = true;
	}
	
	if (tempColor.a > 1f)
	{
		isZeroBlink = false;
	}
	
	if (isZeroBlink)
	{	
		kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,0f,0f,tempColor.a + kekkaiBlinkingSpeed * Time.deltaTime));
	}
	else
	{
		kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,1f,1f,tempColor.a - kekkaiBlinkingSpeed * Time.deltaTime));
	}

	tempColor = kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.GetColor("_TintColor");

	for (var i : int = 0; i < enemies.transform.childCount; i++)
	{
		if (i == 0 || i == 1)
		{
			for (var child : Transform in enemies.transform.GetChild(0).gameObject.transform)
			{
				//print(child.transform.position);
				
				if (child.transform.position.y < 2.5f)
				{
					//print("Seep");
				
					kekkaiBlinkingSpeed = 2f;
				
					kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,0f,0f,tempColor.a));
					
					break;
				}
				else
				{
					//print("Not Seep");
				
					kekkaiBlinkingSpeed = 1f;
				
					kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,1f,1f,tempColor.a));
					
					break;
				}
				
				if (child == enemies.transform.GetChild(0).gameObject.transform.childCount-1)
				{
					for (var child : Transform in enemies.transform.GetChild(1).gameObject.transform)
					{
						//print(child.transform.position);
						
						if (child.transform.position.y < 2.5f)
						{
							//print("Seep");
						
							kekkaiBlinkingSpeed = 4f;
						
							kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,0f,0f,tempColor.a));
							
							break;
						}
						else
						{
							//print("Not Seep");
						
							kekkaiBlinkingSpeed = 2f;
						
							kekkaiGameObject.GetComponent(SkinnedMeshRenderer).material.SetColor("_TintColor", new Color(1f,1f,1f,tempColor.a));
							
							break;
						}
					}
				}
			}
		}
	}

	if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Backspace)) 
    {
    	if (isCountDownFinised)
    	{
	    	if (GameObject.Find("Pauses").transform.position.y > 19.9f)
	    	{
	    		ShowPauseScreen();
	    	}
	    	else
	    	if (GameObject.Find("Pauses").transform.position.y < 10.1f)
	    	{
	    		HidePauseScreen();
	    	}
    	}
    }

	//DEMO purpose
	/*if(Input.GetMouseButtonDown(1)) //if tap the screen with too fingers
	{
		Application.LoadLevel(Application.loadedLevel);
	}*/
}

function CountDown ()
{
	//play sound
	GameObject.Find("Countdown_SFX").GetComponent(AudioSource).Play();

	HOTween.To(GameObject.Find("3").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseInBounce, 0f);

	yield WaitForSeconds(0.75f);
	
	HOTween.To(GameObject.Find("3").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseOutBounce, 0f);
	
	yield WaitForSeconds(0.25f);
	
	HOTween.To(GameObject.Find("2").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseInBounce, 0f);

	yield WaitForSeconds(0.75f);
	
	HOTween.To(GameObject.Find("2").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseOutBounce, 0f);
	
	yield WaitForSeconds(0.25f);
	
	HOTween.To(GameObject.Find("1").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseInBounce, 0f);

	yield WaitForSeconds(0.75f);
	
	HOTween.To(GameObject.Find("1").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseOutBounce, 0f);
	
	yield WaitForSeconds(0.25f);
	
	HOTween.To(GameObject.Find("Shoot").transform, 0.25f, "position", Vector3(0,-10f,0), true, EaseType.EaseInBounce, 0f);

	yield WaitForSeconds(0.75f);
	
	HOTween.To(GameObject.Find("Shoot").transform, 0.25f, "position", Vector2(0,-10), true, EaseType.EaseOutBounce, 0f);
	
	isGameStarted = true;
	
	isCountDownFinised = true;
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(false);
	
	//reset balloon dialogue
	HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(-1.6f,-1.6f,-1.6f), true, EaseType.EaseOutBack, 0f);	
	//GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.transform.localPosition.y = 2.42f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).characterSize = 0.3f;
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").transform.FindChild("Text").gameObject.GetComponent(TextMesh).text = "Shoot!";
	
	//reset the countdown element position
	GameObject.Find("Shoot").transform.position.y = 10.66;
	GameObject.Find("1").transform.position.y = 10.66;
	GameObject.Find("2").transform.position.y = 10.66;
	GameObject.Find("3").transform.position.y = 10.66;
	
	GameObject.Find("BGM_Gameplay").GetComponent(AudioSource).Play();
}

function ShootCannon1 (id : int)
{
	GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(false);

	GameObject.Find("Cannon_1").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
	GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(true);

	GameObject.Find("Bullet_" + id).GetComponent(Bullet).speed = 20;
	
	if (is3ConsecutiveCannonOwned)
	{
		is3ConsecutiveCannonOwned = false;
	
		yield WaitForSeconds(0.5f);
		
		GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(false);
		
		GameObject.Find("Cannon_1").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
		GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(true);

		GameObject.Find("Bullet_" + (id+2)).GetComponent(Bullet).speed = 20;
		
		yield WaitForSeconds(0.5f);
		
		GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(false);
		
		GameObject.Find("Cannon_1").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
		GameObject.Find("Cannon_1").transform.FindChild("VFX").gameObject.SetActive(true);

		GameObject.Find("Bullet_" + (id+4)).GetComponent(Bullet).speed = 20;
	}
}

function ShootCannon2 (id : int)
{
	GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(false);

	GameObject.Find("Cannon_2").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
	GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(true);
	
	GameObject.Find("Bullet_" + id).GetComponent(Bullet).speed = 20;
	
	if (is3ConsecutiveCannonOwned)
	{
		is3ConsecutiveCannonOwned = false;
	
		yield WaitForSeconds(0.5f);
		
		GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(false);
		
		GameObject.Find("Cannon_2").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
		GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(true);

		GameObject.Find("Bullet_" + (id+2)).GetComponent(Bullet).speed = 20;
		
		yield WaitForSeconds(0.5f);
		
		GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(false);
		
		GameObject.Find("Cannon_2").transform.FindChild("Cannon").gameObject.GetComponent(Animator).CrossFade ("Shoot", 0.01f);
		GameObject.Find("Cannon_2").transform.FindChild("VFX").gameObject.SetActive(true);

		GameObject.Find("Bullet_" + (id+4)).GetComponent(Bullet).speed = 20;
	}
}

function ResetEnemies ()
{
	enemies.SetActive(false);

	var enemy = Instantiate(enemiesDummy, Vector3(0, 0, 0), Quaternion.identity);

	enemy.name = "Enemies";
	
	//positioning
	enemy.transform.localPosition.x = 0;
	enemy.transform.localPosition.y = 2.35;
	enemy.transform.localPosition.z = -4.84;
	
	//rotating
	enemy.transform.localRotation.x = 0;
	enemy.transform.localRotation.y = 0;
	enemy.transform.localRotation.z = 0;
	
	enemies = enemy;
	
	enemies.SetActive(true);
	
	yield WaitForSeconds(0.5f);
	
	ActivateAnimatorForEnemiesLeft();
	ActivateAnimatorForEnemiesRight();
}

function ResetScaleDialogueBalloon ()
{
	yield WaitForSeconds(0.25f);
	
	//print("Reset!");
	
	//GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform.localScale = new Vector3(0f,0f,0f);
	
	HOTween.To(GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform, 0.25f, "localScale", Vector3(-0.7f,-0.7f,-0.7f), true, EaseType.EaseInBack, 0f);

	yield WaitForSeconds(0.25f);
	
	GameObject.Find("Dialogue_Balloon_Shoot").transform.FindChild("Armature.003").gameObject.transform.FindChild("Armature.003|Bone").gameObject.transform.localScale = new Vector3(0f,0f,0f);
}

function ResetAllNumberButtons ()
{
	GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(MeshRenderer).material = unselectedMonitorMaterial;
	
	GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(MeshRenderer).material = unselectedMonitorMaterial;
	
	GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(MeshRenderer).material = unselectedMonitorMaterial;
	
	GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(MeshRenderer).material = unselectedMonitorMaterial;
	
	GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(MeshRenderer).material = unselectedMonitorMaterial;
	
	GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).isSelected = false;
	GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).isSelected = false;
	GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).isSelected = false;
	GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).isSelected = false;
	GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).isSelected = false;
	
	GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = 0;
	GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = 0;
	GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = 0;
	GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = 0;
	GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = 0;
}

function ActivateAnimatorForEnemiesRight ()
{
	yield WaitForSeconds(0.5f);

	for (var i : int = 0; i < GameObject.Find("Enemy_Right").transform.childCount; i++)
	{
		GameObject.Find("Enemy_Right").transform.GetChild(i).gameObject.transform.FindChild("Alien").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	
		yield WaitForSeconds(0.5f);
	}
}

function ActivateAnimatorForEnemiesLeft ()
{
	for (var j : int = 0; j < GameObject.Find("Enemy_Left").transform.childCount; j++)
	{
		GameObject.Find("Enemy_Left").transform.GetChild(j).gameObject.transform.FindChild("Alien").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	
		yield WaitForSeconds(0.5f);
	}
}

function GenerateNumbersForAliens ()
{
	for (var i : int = 0; i < 68; i++)
	{
		GameObject.Find("Enemy_Right").transform.GetChild(i).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text = "" + Random.Range(7,13);
	}
	
	for (var j : int = 0; j < 56; j++)
	{
		GameObject.Find("Enemy_Left").transform.GetChild(j).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text = "" + Random.Range(7,13);
	}
}

function GenerateNewNumber (id : int) //0 for beginning, 1 after hitting right, 2 after hitting left
{
	ResetAllNumberButtons();

	totalNumberSelected = 0;

	isTimerForBlinkStarted = true; //start the timer

	if (id == 0)
	{
		number1NeedToMatch = int.Parse(GameObject.Find("Enemy_Left").transform.GetChild(lastEnemyLeftIndexToHit).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text);
	
		number2NeedToMatch = int.Parse(GameObject.Find("Enemy_Right").transform.GetChild(lastEnemyRightIndexToHit).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text);
	}
	else
	if (id == 2)
	{
		Debug.Log("Generate Left Number");
	
		number1NeedToMatch = int.Parse(GameObject.Find("Enemy_Left").transform.GetChild(lastEnemyLeftIndexToHit).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text);
	}
	else
	if (id == 1)
	{
		Debug.Log("Generate Right Number");
	
		number2NeedToMatch = int.Parse(GameObject.Find("Enemy_Right").transform.GetChild(lastEnemyRightIndexToHit).gameObject.transform.FindChild("Number_Text").gameObject.GetComponent(TextMesh).text);
	}

	if (number1NeedToMatch >= number2NeedToMatch)
	{
		var number1 : int = Random.Range(2,number2NeedToMatch-2);
	}
	else
	{
		number1 = Random.Range(2,number1NeedToMatch-2);
	}
	
	var number2 : int = number1NeedToMatch - number1;
	var number3 : int = number2NeedToMatch - number1;
	var number4 : int = number1 + 2;
	var number5 : int = number1 - 1;
	
	var index : int = Random.Range(1,5);
	
	if (index == 1)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		
		solution1 = GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject;
		solution2_1 = GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject;
		solution2_2 = GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject;
	}
	else
	if (index == 2)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		
		solution1 = GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject;
		solution2_1 = GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject;
		solution2_2 = GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject;
	}
	else
	if (index == 3)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		
		solution1 = GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject;
		solution2_1 = GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject;
		solution2_2 = GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject;
	}
	else
	if (index == 4)
	{
		GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number2;
		GameObject.Find("Monitor_2").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number4;
		GameObject.Find("Monitor_3").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number5;
		GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number3;
		GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject.GetComponent(Number_Button).number = number1;
		
		solution1 = GameObject.Find("Monitor_5").transform.FindChild("Monitor").gameObject;
		solution2_1 = GameObject.Find("Monitor_1").transform.FindChild("Monitor").gameObject;
		solution2_2 = GameObject.Find("Monitor_4").transform.FindChild("Monitor").gameObject;
	}
}

function ShowResultScreen ()
{
	isGameStarted = false;
	isCountDownFinised = false;
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(true);
	
	GameObject.Find("Current_Score_Text").GetComponent(TextMesh).text = "" + currentScore;
	
	GameObject.Find("Kekkais").transform.FindChild("Kekkai").gameObject.GetComponent(Animator).CrossFade ("Collapse", 0.01f);
	GameObject.Find("Professor_Cursing").transform.FindChild("Cursing").gameObject.GetComponent(Animator).CrossFade ("Dead", 0.01f);
	GameObject.Find("Professor_Right").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Dead", 0.01f);
	GameObject.Find("Professor_Left").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Dead", 0.01f);
	
	//yield WaitForSeconds(1f);
	
	HOTween.To(GameObject.Find("Results").transform, 1.25f, "position", Vector3(0,-10f,0f), true, EaseType.EaseInBack, 0f);
	
	if (currentScore > PlayerPrefs.GetInt("Highscore"))
	{
		//set a new highscore
		PlayerPrefs.SetInt("Highscore", currentScore);
		
		GameObject.Find("Text_4").GetComponent(TextMesh).text = "NEW\nHIGHSCORE!";
		
		GameObject.Find("Text_4").GetComponent(MeshRenderer).material.color.r = 1.0f;
		GameObject.Find("Text_4").GetComponent(MeshRenderer).material.color.g = 0f;
		GameObject.Find("Text_4").GetComponent(MeshRenderer).material.color.b = 0f;
	}
	
	GameObject.Find("Highscore_Text").GetComponent(TextMesh).text = "" + PlayerPrefs.GetInt("Highscore");
	
	yield WaitForSeconds(1.25f);
	
	GameObject.Find("BGM_Lose").GetComponent(AudioSource).Play();
}

function ShowPauseScreen ()
{
	HOTween.To(GameObject.Find("Pauses").transform, 0.25f, "position", Vector3(0,-10f,0f), true, EaseType.EaseOutBack, 0f);
	
	yield WaitForSeconds(0.25f);
	
	isGameStarted = false;
}

function HidePauseScreen ()
{
	HOTween.To(GameObject.Find("Pauses").transform, 0.25f, "position", Vector3(0,10f,0f), true, EaseType.EaseInBack, 0f);
	
	yield WaitForSeconds(0.25f);
	
	isGameStarted = true;
}

function RunTimerForBlink ()
{
	if (timerForBlink < 2.0f)
	{
		timerForBlink = timerForBlink + Time.deltaTime;
	}
	else
	{
		isTimerForBlinkStarted = false; //stop the timer
		
		BlinkNumberButtons ();
	}
}

function BlinkNumberButtons ()
{
	solution1.GetComponent(MeshRenderer).material= selectedMonitorMaterial;
	
	yield WaitForSeconds(0.25f);
	
	if (solution1.GetComponent(Number_Button).isSelected == false)
	{
		solution1.GetComponent(MeshRenderer).material= unselectedMonitorMaterial;
	}
	
	yield WaitForSeconds(0.25f);
	
	solution1.GetComponent(MeshRenderer).material= selectedMonitorMaterial;
	
	yield WaitForSeconds(0.25f);
	
	if (solution1.GetComponent(Number_Button).isSelected == false)
	{
		solution1.GetComponent(MeshRenderer).material= unselectedMonitorMaterial;
	}
	
	yield WaitForSeconds(0.25f);
	
	solution1.GetComponent(MeshRenderer).material= selectedMonitorMaterial;
	
	yield WaitForSeconds(0.25f);
	
	if (solution1.GetComponent(Number_Button).isSelected == false)
	{
		solution1.GetComponent(MeshRenderer).material= unselectedMonitorMaterial;
	}
}