﻿#pragma strict

public var selectedMaterial : Material;
public var unselectedMaterial : Material;


function Start () 
{

}

function Update () 
{

}

function OnMouseDown ()
{
	print("Mouse Down on Exit Button!");
	
	GameObject.Find("Button_Active_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 1.0f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 1.0f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 1.0f;
	
	this.gameObject.GetComponent(MeshRenderer).material = selectedMaterial;
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Exit Button!");
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(false);
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
	
	//Application.LoadLevel("Title");
	
	GameObject.Find("Kekkais").transform.FindChild("Kekkai").gameObject.GetComponent(Animator).CrossFade ("None", 0.01f);
	GameObject.Find("Professor_Cursing").transform.FindChild("Cursing").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	GameObject.Find("Professor_Right").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	GameObject.Find("Professor_Left").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Idle", 0.01f);
	
	HOTween.To(GameObject.Find("Pauses").transform, 0.5f, "position", Vector3(0,10f,0f), true, EaseType.EaseInBack, 0f);
	
	HOTween.To(GameObject.Find("Results").transform, 0.5f, "position", Vector3(0,10f,0f), true, EaseType.EaseInBack, 0f);
	
	GameObject.Find("BGM_Gameplay").GetComponent(AudioSource).Stop();
	
	GameObject.Find("BGM_Lose").GetComponent(AudioSource).Stop();
	
	HOTween.To(GameObject.Find("Enemies").transform, 0.5f, "localPosition", Vector3(0f,2.35f,-4.84f), false, EaseType.EaseInBack, 0f);
	HOTween.To(GameObject.Find("Enemy_Right").transform, 0.5f, "localPosition", Vector3(0.76f,-2.48f,0.15f), false, EaseType.EaseInBack, 0f);
	HOTween.To(GameObject.Find("Enemy_Left").transform, 0.5f, "localPosition", Vector3(-0.76f,-2.48f,0.15f), false, EaseType.EaseInBack, 0f);
	
	HOTween.To(GameObject.Find("Score_Bar").transform, 0.5f, "localPosition", Vector3(0f, 1f,0f), true, EaseType.EaseInBack, 0f);
	
	GameObject.Find("Kekkai").transform.FindChild("Collider").gameObject.SetActive(false);
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).currentScore = 0f;
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).isGameStarted = false;
	
	yield WaitForSeconds(0.5f);
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).ResetEnemies();
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNumbersForAliens();
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyLeftIndexToHit = 0;
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyRightIndexToHit = 0;
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(0); //reset number
	
	HOTween.To(GameObject.Find("Titles").transform, 0.5f, "localPosition", Vector3(0f,-5f,0f), true, EaseType.EaseOutBack, 0f);
	
	GameObject.Find("BGM_Title").GetComponent(AudioSource).Play();
}

function OnMouseUp ()
{
	print("Mouse Up on Exit Button!");
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	GameObject.Find("Exit").transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
}