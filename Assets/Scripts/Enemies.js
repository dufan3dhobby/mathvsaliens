﻿#pragma strict

public var speed : float;

private var gameplayController : Gameplay_Controller;

function Start () 
{
	gameplayController = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller);	
}

function Update () 
{
	if (gameplayController.isGameStarted)
	{
		//move this downward
		this.gameObject.transform.position.y = this.gameObject.transform.position.y - speed * Time.deltaTime;
	}
}