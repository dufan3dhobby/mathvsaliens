﻿#pragma strict

function Start () 
{

}

function Update () 
{

}

function OnMouseDown ()
{
	print("Mouse Down on Tutorial Button!");
	
	this.gameObject.transform.localScale.x = this.gameObject.transform.localScale.x * 1.1f;
	this.gameObject.transform.localScale.y = this.gameObject.transform.localScale.y * 1.1f;
	this.gameObject.transform.localScale.z = this.gameObject.transform.localScale.z * 1.1f;
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Tutorial Button!");
	
	this.gameObject.transform.localScale.x = 0.8f;
	this.gameObject.transform.localScale.y = 0.8f;
	this.gameObject.transform.localScale.z = 0.8f;
	
	GameObject.Find("Title_Controller").GetComponent(Title_Controller).ShowTutorialScreen();
}

function OnMouseUp ()
{
	print("Mouse Up on Tutorial Button!");
	
	this.gameObject.transform.localScale.x = 0.8f;
	this.gameObject.transform.localScale.y = 0.8f;
	this.gameObject.transform.localScale.z = 0.8f;
}