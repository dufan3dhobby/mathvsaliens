﻿#pragma strict

public var selectedMaterial : Material;
public var unselectedMaterial : Material;


function Start () 
{

}

function Update () 
{

}

function OnMouseDown ()
{
	print("Mouse Down on Retry Button!");
	
	GameObject.Find("Button_Active_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 1.0f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 1.0f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 1.0f;
	
	this.gameObject.GetComponent(MeshRenderer).material = selectedMaterial;
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Retry Button!");
	
	GameObject.Find("Main Camera").transform.FindChild("Collider").gameObject.SetActive(false);
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
	
	GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).HidePauseScreen();
}

function OnMouseUp ()
{
	print("Mouse Up on Retry Button!");
	
	GameObject.Find("Button_Inactive_Other_SFX").GetComponent(AudioSource).Play();
	
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.r = 0.816f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.g = 0.423f;
	this.gameObject.transform.FindChild("Text").gameObject.GetComponent(MeshRenderer).material.color.b = 0.141f;
	
	this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
}