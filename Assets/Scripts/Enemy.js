﻿#pragma strict

function Start () 
{

}

function Update () 
{
	/*if (this.gameObject.name == "Enemy_1" && this.gameObject.transform.parent.gameObject.name == "Enemy_Left")
	{
		print("Enemy: " + this.gameObject.transform.TransformPoint(0, 0, 0).y);
	}*/
	
	/*if (this.gameObject.transform.TransformPoint(0, 0, 0).y < 7f)
	{
		//this.gameObject.transform.FindChild("Alien").gameObject.GetComponent(Animator).enabled = true;
		
		print("Oke");
	}*/
}

/*function OnCollisionEnter(collision: Collision) 
{
	//print("Collision: " + collision.gameObject.tag);
	
	if (collision.gameObject.tag == "Bullet")
	{
		Destroy(this.gameObject);
		
		//make the enemies faster
		GameObject.Find("Enemies").GetComponent(Enemies).speed = GameObject.Find("Enemies").GetComponent(Enemies).speed * 1.1f;
		
		//stop the bullet from movement
		collision.gameObject.GetComponent(Bullet).speed = 0;
		collision.gameObject.GetComponent(Bullet).MoveBullet();
		
		//reset to 0
		GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected = 0;
		
		//generate new number
		GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber();
	}
}*/

function OnTriggerEnter (other : Collider)
{
	if (other.gameObject.tag == "Bullet")
	{
		/*if (this.gameObject.name == "Upper_Line")
		{
			Debug.Log("Kena Upper Line");
		
			//stop the bullet from movement
			other.gameObject.GetComponent(Bullet).speed = 0;
			other.gameObject.GetComponent(Bullet).MoveBullet();
		}
		else
		{*/
			if (this.gameObject.transform.parent.gameObject.name == "Enemy_Left")
			{
				GameObject.Find("Professor_Left").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Happy", 0.01f);
			}
			else
			{
				GameObject.Find("Professor_Right").transform.FindChild("Prof").gameObject.GetComponent(Animator).CrossFade ("Happy", 0.01f);
			}
		
			GameObject.Find("Explode_SFX").GetComponent(AudioSource).Play();
		
			//if (this.gameObject.name != "Enemy_Strong")
			//{
			this.gameObject.transform.FindChild("VFX").gameObject.SetActive(true);
			this.gameObject.transform.FindChild("VFX").gameObject.transform.parent = null;
			
			//}
			/*else
			{
				if (this.gameObject.transform.FindChild("HP").gameObject != null)
				{
					Debug.Log("Reduce HP");
				
					this.gameObject.transform.FindChild("VFX").gameObject.SetActive(true);
					this.gameObject.transform.FindChild("VFX").gameObject.transform.parent = null;
				
					Destroy(this.gameObject.transform.FindChild("HP").gameObject);
				}
				else
				{
					this.gameObject.transform.FindChild("VFX").gameObject.SetActive(true);
					this.gameObject.transform.FindChild("VFX").gameObject.transform.parent = null;
				
					Destroy(this.gameObject);
				}
			}*/
			
			if (this.gameObject.transform.parent.gameObject.GetComponent(Enemies).speed < 0.75f)
			{
				//make the enemies faster
				this.gameObject.transform.parent.gameObject.GetComponent(Enemies).speed = this.gameObject.transform.parent.gameObject.GetComponent(Enemies).speed * 1.1f;
			}
			else
			{
				HOTween.To(GameObject.Find("Enemies").transform, 0.5f, "localPosition", Vector3(0f,1f,0f), true, EaseType.EaseInBack, 0f);
			
				this.gameObject.transform.parent.gameObject.GetComponent(Enemies).speed = 0.375f;
			}
			
			//stop the bullet from movement and reset its position
			other.gameObject.GetComponent(Bullet).speed = 0;
			other.gameObject.GetComponent(Bullet).ResetPosition();
			
			//generate new number
			if (this.gameObject.transform.parent.gameObject.name == "Enemy_Left")
			{
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyLeftIndexToHit = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyLeftIndexToHit + 1;
			
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(2);
			}
			else
			{
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyRightIndexToHit = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).lastEnemyRightIndexToHit + 1;
			
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).GenerateNewNumber(1);
			}
			
			//Destroy(this.gameObject);
			
			this.gameObject.SetActive(false);
			
			yield WaitForSeconds(0.5f);
			
			Destroy(this.gameObject);
			
			/*if (this.gameObject.name == "Enemy_1")
			{
				print("Musuh ijo");
				
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).comboChecker.Push(1);
			}
			
			if (this.gameObject.name == "Enemy_2")
			{
				print("Musuh biru");
			
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).comboChecker.Push(2);
			}
			
			if (this.gameObject.name == "Enemy_3")
			{
				print("Musuh merah");
			
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).comboChecker.Push(3);
			}*/
			
			/*if (this.gameObject.name == "Enemy_Strong")
			{
				print("Musuh kuat");
			
				GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).comboChecker.Push(4);
			}*/
		//}
	}
	else
	if (other.gameObject.tag == "Kekkai")
	{
		print("Kekkai!");
	
		GameObject.Find("BGM_Gameplay").GetComponent(AudioSource).Stop();
		GameObject.Find("Kekkai_Break_SFX").GetComponent(AudioSource).Play();
	
		GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).ShowResultScreen();
	}
}