﻿#pragma strict

public var number : int;

public var selectedMaterial : Material;
public var unselectedMaterial : Material;

public var isSelected : boolean;

function Start () 
{

}

function Update () 
{
	this.gameObject.transform.parent.FindChild("Number").gameObject.GetComponent(TextMesh).text = "" + number;
}

function OnMouseDown ()
{
	print("Mouse Down on Number Button!" + Application.loadedLevel);
	
	if (GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).isGameStarted)
	{	
		if (!isSelected)
		{
			isSelected = true;
		
			GameObject.Find("Button_Active_SFX").GetComponent(AudioSource).Play();
		
			this.gameObject.GetComponent(MeshRenderer).material = selectedMaterial;
		
			//set color the material
			//this.gameObject.GetComponent(MeshRenderer).material.color.r = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.g = 0f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.b = 0f;
		
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected + number;
		}
		else
		{
			isSelected = false;
		
			GameObject.Find("Button_Inactive_SFX").GetComponent(AudioSource).Play();
		
			this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
		
			//set color the material
			//this.gameObject.GetComponent(MeshRenderer).material.color.r = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.g = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.b = 1f;
		
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected - number;
		}
	}
	else
	{
		if (!isSelected)
		{
			isSelected = true;
		
			GameObject.Find("Button_Active_SFX").GetComponent(AudioSource).Play();
		
			this.gameObject.GetComponent(MeshRenderer).material = selectedMaterial;
		
			//set color the material
			//this.gameObject.GetComponent(MeshRenderer).material.color.r = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.g = 0f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.b = 0f;
		
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected + number;
		}
		else
		{
			isSelected = false;
		
			GameObject.Find("Button_Inactive_SFX").GetComponent(AudioSource).Play();
		
			this.gameObject.GetComponent(MeshRenderer).material = unselectedMaterial;
		
			//set color the material
			//this.gameObject.GetComponent(MeshRenderer).material.color.r = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.g = 1f;
			//this.gameObject.GetComponent(MeshRenderer).material.color.b = 1f;
		
			GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected = GameObject.Find("Gameplay_Controller").GetComponent(Gameplay_Controller).totalNumberSelected - number;
		}
	}
}

function OnMouseUpAsButton ()
{
	print("Mouse Up As Button on Number Button!");
}

function OnMouseUp ()
{
	print("Mouse Up on Number Button!");
}